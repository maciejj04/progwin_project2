import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by student on 2017-04-07.
 */
public class FileData {
    private ArrayList<Double> X = new ArrayList<>();
    private TreeMap<String,ArrayList<Double>> Y = new TreeMap<>();
    private Double START_M;
    private Double STOP_M;
    private Double STEP_M;
    private Double NULL;

    public ArrayList<Double> getX() {
        return X;
    }

    public void addX(Double x) {
        X.add(x);
    }

    public TreeMap<String, ArrayList<Double>> getY() {
        return Y;
    }

    public void setY(TreeMap<String, ArrayList<Double>> y) {
        Y = y;
    }

    public void addValueInY(String key,Double value){
        Y.get(key).add(value);
    }

    public Double getSTART_M() {
        return START_M;
    }

    public void setSTART_M(Double START_M) {
        this.START_M = START_M;
    }

    public Double getSTOP_M() {
        return STOP_M;
    }

    public void setSTOP_M(Double STOP_M) {
        this.STOP_M = STOP_M;
    }

    public Double getSTEP_M() {
        return STEP_M;
    }

    public void setSTEP_M(Double STEP_M) {
        this.STEP_M = STEP_M;
    }

    public Double getNULL() {
        return NULL;
    }

    public void setNULL(Double NULL) {
        this.NULL = NULL;
    }
}
