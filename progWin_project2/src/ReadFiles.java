import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by student on 2017-04-07.
 */
public class ReadFiles {
    private final String file1Path = "W.LAS";
    private final String file2Path = "K.las";
    private final String file3Path = "SOB.LAS";


    private FileData file1Data = new FileData();
    private FileData file2Data = new FileData();
    private FileData file3Data = new FileData();

    public ReadFiles() {
        readFiles(file1Path, file1Data);
        readFiles(file2Path, file2Data);
        readFiles(file3Path, file3Data);
    }
    private void readFiles(String file,FileData fData){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        findParamInFile(br,"STRT.M",fData);
        findParamInFile(br,"STOP.M",fData);
        findParamInFile(br,"STEP.M",fData);
        findParamInFile(br,"NULL.", fData);

        setXandY(br,fData);

    }
    private void setXandY(BufferedReader br,FileData fData){
        String line = null;
        String[] columnNames = null;
        try {
            while( true ) {
                line = br.readLine();
                if (line.startsWith("~A")) {
                    columnNames = line.split("\\s+");
                    break;
                }
            }

            br.readLine();//because we got " # M" single line in file before numbers;

            while((line = br.readLine()) != null){
                String[] numbers = line.split("\\s+");

                fData.addX(Double.valueOf(numbers[1]));
                for (int i = 2;i < numbers.length; i++){
                    ArrayList<Double> definitions = fData.getY().get(columnNames[i]);
                    if( definitions == null){
                        definitions = new ArrayList<>();
                        fData.getY().put(columnNames[i], definitions);
                    }
                    definitions.add(Double.valueOf(numbers[i]));
                    //fData.getY().put(columnNames[2],Double.valueOf(numbers[2]));
                    //fData.addValueInY(columnNames[i],Double.valueOf(numbers[i]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private void findParamInFile(BufferedReader br,String textToFind, FileData fileData){
        String line = null;

        try {
            while( true ) {
                line = br.readLine();
                if (line.contains(textToFind)) {
                    switch (textToFind){
                        case "STRT.M":
                            fileData.setSTART_M(Double.valueOf(line.substring(7, line.indexOf(":")).trim()));
                            return;
                        case "STOP.M":
                            fileData.setSTOP_M(Double.valueOf(line.substring(7, line.indexOf(":")).trim()));
                            return;
                        case "STEP.M":
                            fileData.setSTEP_M(Double.valueOf(line.substring(7, line.indexOf(":")).trim()));
                            return;
                        case "NULL.":
                            fileData.setNULL(Double.valueOf(line.substring(7, line.indexOf(":")).trim()));
                            return;
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileData getFile1Data() {
        return file1Data;
    }

    public FileData getFile2Data() {
        return file2Data;
    }

    public FileData getFile3Data() {
        return file3Data;
    }

}
