import javax.swing.*;
import java.awt.*;

/**
 * Created by student on 2017-04-07.
 */
public class myPanel extends JPanel {

    private FileData currentFileData = null;
    private String currentColumn = null;
    private appWin parent = null;

    public myPanel(FileData currentFileData,appWin parent) {
        this.parent = parent;
        this.currentFileData = currentFileData;
        this.currentColumn = currentFileData.getY().firstKey();

        System.out.println(this.currentColumn);
        System.out.println(this.currentFileData);


        setBackground(new Color(255,235,200));
        setLayout(new BorderLayout());


//        JScrollBar scrollBar = new JScrollBar();
//        scrollBar.setUnitIncrement(2);
//        scrollBar.addAdjustmentListener((ae)->{
//            System.out.println("dupcia");
//            repaint();
//        });
//
//
//        add(scrollBar,BorderLayout.EAST);
        //setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if(!parent.isLogarythmic()) graphics.drawString(currentFileData.getX().get(0).toString(),3,20);
        if(!parent.isLogarythmic()) graphics.drawString(currentFileData.getX().get(currentFileData.getX().size()-1).toString(),3,getHeight()-20);
        graphics.drawRect(55,10,getWidth()-70,getHeight()-20);// jesli zmieniasz to 55 to zmien również w fcji obliczajacej wykres!

        for(int i = 100;i<getHeight()-99;i+=100){
            graphics.drawString(currentFileData.getX().get(i).toString(),5,i);
            graphics.drawLine(55,i,getWidth()-15,i);
        }
        for(int i = 100;i<getHeight()-99;i+=100) {
            if(parent.isLogarythmic()){
                Double min = currentFileData.getY().get(currentColumn).stream().filter((ar)->{return !ar.equals(currentFileData.getNULL());}).min(Double::compare).get();
                Double max = currentFileData.getY().get(currentColumn).stream().filter((ar)->{return !ar.equals(currentFileData.getNULL());}).max(Double::compare).get();

                min = Math.log10(min);
                max = Math.log10(max);
                Double y = Math.log10(i);

                Double coordinate = (  ( (getWidth()-70-50)/(max-min) )*(y-min)  )+50;
                graphics.drawString(coordinate.toString().substring(0,8),i+40,9);
            }else{
                graphics.drawString(currentFileData.getY().get(currentColumn).get(i).toString(),i+40,9);
            }
            graphics.drawLine(55+i,10,55+i,getHeight()-10);
        }

        drawChart(graphics);
    }


    public FileData getCurrentFileData() {
        return currentFileData;
    }

    public void setCurrentFileData(FileData currentFileData) {
        this.currentFileData = currentFileData;
    }

    public String getCurrentColumn() {
        return currentColumn;
    }

    public void setCurrentColumn(String currentColumn) {
        this.currentColumn = currentColumn;
        System.out.println(this.currentColumn);
    }

    private int countPixelForY(Double y) {

        Double min = currentFileData.getY().get(currentColumn).stream().filter((ar)->{return !ar.equals(currentFileData.getNULL());}).min(Double::compare).get();
        Double max = currentFileData.getY().get(currentColumn).stream().filter((ar)->{return !ar.equals(currentFileData.getNULL());}).max(Double::compare).get();

        if(parent.isLogarythmic()){
            min = Math.log10(min);
            max = Math.log10(max);
            y = Math.log10(y);
        }

            Double coordinate = (  ( (getWidth()-70-50)/(max-min) )*(y-min)  )+50;
            int res = coordinate.intValue();
            return res;
    }
    private void drawChart(Graphics g){
        for ( int  i = 0; i < currentFileData.getX().size()-1; i++ ){
            Double y = currentFileData.getY().get(currentColumn).get(i);
            Double y2 = currentFileData.getY().get(currentColumn).get(i+1);
            Double nullDigit = currentFileData.getNULL();

                if( y.equals(nullDigit)|| y2.equals(nullDigit) )
                    continue;

                g.drawLine(countPixelForY(y),i+10,countPixelForY(y2),i+11);
        }

    }

}
