import javax.swing.*;
import java.awt.*;

/**
 * Created by student on 2017-04-07.
 */
public class appWin extends JFrame{
    private String[] files = {"W.LAS","K.LAS","SOB.LAS"};
    private String[] scaleTypes = {"logarytmiczna","liniowa"};
    private JComboBox<String> filesCB = new JComboBox<>(files);
    private JComboBox<String> columnsCB = new JComboBox<>();

    public JComboBox<String> getScaleTypeCB() {
        return scaleTypeCB;
    }

    private JComboBox<String> scaleTypeCB = new JComboBox<>(scaleTypes);

    private myPanel mainPanel = null;

    private ReadFiles rf = new ReadFiles();

    private boolean columnsCBactionFlag = true;


    public appWin() {
        setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(700,700));

        scaleTypeCB.setSelectedIndex(1);

        mainPanel = new myPanel(rf.getFile1Data(),this);
        for(String i : rf.getFile1Data().getY().keySet())
            columnsCB.addItem(i);


        filesCB.addActionListener((ae)->{
            columnsCBactionFlag = false;
            setCurrentFile();
            mainPanel.setPreferredSize(new Dimension(700,mainPanel.getCurrentFileData().getX().size()+20));//+20 bo kwadrat jest rysowany od wys 10 do getHeight()-10
            System.out.println(mainPanel.getCurrentFileData().getX().size());//TODO:
            columnsCB.removeAllItems();

            for(String i : mainPanel.getCurrentFileData().getY().keySet())
                columnsCB.addItem(i);

            columnsCB.setSelectedIndex(0);
            mainPanel.setCurrentColumn(columnsCB.getSelectedItem().toString());
            columnsCBactionFlag = true;
            mainPanel.revalidate();
            mainPanel.repaint();
            repaint();
        });
        columnsCB.addActionListener((ae)-> {
            if (columnsCBactionFlag){
                mainPanel.setCurrentColumn(columnsCB.getSelectedItem().toString());
                System.out.println(mainPanel.getCurrentFileData().getX().size());//TODO:asd
                mainPanel.setPreferredSize(new Dimension(700,mainPanel.getCurrentFileData().getX().size()+20));//+20 bo kwadrat jest rysowany od wys 10 do getHeight()-10
                mainPanel.revalidate();
                mainPanel.repaint();
                repaint();
            }
        });
        scaleTypeCB.addActionListener((ae)->{
            if (scaleTypeCB.getSelectedItem().toString().equals("logarytmiczna")){
                //Double min = mainPanel.getCurrentFileData().getY().get(mainPanel.getCurrentColumn()).stream().min(Double::compare).get();

                Double min = mainPanel.getCurrentFileData().getY().get(mainPanel.getCurrentColumn()).stream().filter((ar)->{return !ar.equals(mainPanel.getCurrentFileData().getNULL());}).min(Double::compare).get();
                System.out.println("-------\n"+filesCB.getSelectedItem().toString());
                System.out.println(columnsCB.getSelectedItem().toString());
                System.out.println(min);

                if(min < 0){
                    JFrame frame = new JFrame();
                    frame.add(new JLabel("Can not convert to logarytmiczna"));
                    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    frame.setSize(new Dimension(300,100));
                    frame.setVisible(true);
                }
                mainPanel.revalidate();
                mainPanel.repaint();
                repaint();
            }

        });

        JPanel panelCB = new JPanel();
        panelCB.setLayout(new BoxLayout(panelCB,BoxLayout.X_AXIS));
        panelCB.setMaximumSize(new Dimension(this.getWidth(),30));
        panelCB.add(filesCB);
        panelCB.add(columnsCB);
        panelCB.add(scaleTypeCB);


        JScrollPane scrollPanel = new JScrollPane(mainPanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        add(panelCB);
        add(scrollPanel);
        //add(mainPanel);


        setVisible(true);
    }
    public boolean isLogarythmic(){
        return scaleTypeCB.getSelectedItem().toString().equals("logarytmiczna");
    }
    private void setCurrentFile(){
        switch(filesCB.getSelectedItem().toString()){
            case "W.LAS":
                mainPanel.setCurrentFileData(rf.getFile1Data());
                return;
            case "K.LAS":
                mainPanel.setCurrentFileData(rf.getFile2Data());
                return;
            case "SOB.LAS":
                mainPanel.setCurrentFileData(rf.getFile3Data());
                return;

        }

    }
}
